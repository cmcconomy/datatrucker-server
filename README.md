<h1 align="center">DataTrucker.IO API Server</h1>

<p align="center">
  <img width="300" src="./logo/branding-website_logo.svg?raw=true"/>
</p>

# DataTrucker.IO


## General

DataTrucker.IO is an API Utility that provides the capability to create, host and operationalize APIs with little to no code.

## Documentation

* [Knowledge Base Home](https://www.datatrucker.io/knowledge-base/)
  * [Build & Install](https://www.datatrucker.io/knowledge-base/quick-start-guide/)
  * [Security & Productionalization](https://www.datatrucker.io/knowledge-base/production-build-guide/)


## Updates

Watch our [Blog posts](https://www.datatrucker.io/blog/) for Release Information 

## Support and Questions

We are happy to help, send queries to [support@datatrucker.io](mailto:support@datatrucker.io) or contact us [here](https://www.datatrucker.io/contact-us/)
