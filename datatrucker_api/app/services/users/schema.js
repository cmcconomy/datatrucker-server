/*
* Copyright 2021 Datatrucker.io Inc , Ontario , Canada
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*     http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/ 

const usersSearch = {
      querystring: {
            username: {type: 'string', minimum: 4, pattern: '^[A-Za-z0-9]+$'},
            groupname: {type: 'string', minimum: 4, pattern: '^[A-Za-z0-9]+$'}
      }
};

const userrolesSearch = {
      params: {
            username: {type: 'string', minimum: 4, pattern: '^[A-Za-z0-9]+$'}
      }
};

const users = {
      body: {
            type: 'object',
            required: ['username', 'password', 'enabled'],
            properties: {
                  username: {type: 'string', minimum: 4, pattern: '^[A-Za-z0-9]+$'},
                  password: {
                        type: 'string',
                        pattern: '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})',
                        minimum: 8
                  },
                  emailid: {type: 'string'},
                  enabled: {type: 'boolean'}
            }
      }
};

const usersUpdate = {
      params: {
            username: {type: 'string', minimum: 4, pattern: '^[A-Za-z0-9]+$'}
      },
      body: {
            type: 'object',
            required: ['password', 'enabled'],
            properties: {
                  password: {
                        type: 'string',
                        pattern: '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})',
                        minimum: 8
                  },
                  emailid: {type: 'string'},
                  enabled: {type: 'boolean'}
            }
      }
};

const usersDelete = {
      params: {
            username: {type: 'string', minimum: 4, pattern: '^[A-Za-z0-9]+$'}
      }
};

const usermapping = {
      body: {
            type: 'object',
            required: ['username', 'groupname'],
            properties: {
                  username: {type: 'string', minimum: 4, pattern: '^[A-Za-z0-9]+$'},
                  groupname: {type: 'string', minimum: 4, pattern: '^[A-za-z0-9]+$'}
            }
      }
};

const usermappingDelete = {
      params: {
            id: {type: 'integer'}
      }
};

const userlistings = {
      querystring: {
            username: {type: 'string', minimum: 4, pattern: '^[A-Za-z0-9]+$'}
      }
};

exports.usersSearch = usersSearch;
exports.userrolesSearch = userrolesSearch;
exports.users = users;
exports.usersUpdate = usersUpdate;
exports.usersDelete = usersDelete;
exports.usermapping = usermapping;
exports.usermappingDelete = usermappingDelete;
exports.userlistings = userlistings;
