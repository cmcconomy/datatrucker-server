/*
* Copyright 2021 Datatrucker.io Inc , Ontario , Canada
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*     http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/ 

/* eslint-disable no-await-in-loop */
/* eslint-disable space-infix-ops */
const flatten = require('flat');

async function job(request, reply) {
      request.preJobHandler(request);
      request.template = await this.ResourceConfig(request.user, request.params.jobid, request.method);
      const valid = await request.ajvHandler(request, reply);
      if (valid !== true) {
            reply.code(422).send(reply.replyHandler(valid, request.id));
      } else {
            const data = await this[request.template.type](request, reply);
            reply.send(reply.replyHandler(data, request.id));
      }
}

async function chain(request, reply) {
      request.preJobHandler(request);
      const bodychain = {};
      let flag = true;
      let bodychainindex = 0;
      bodychain[bodychainindex] = request.datacontent;

      request.chaintemplate = await this.ResourceConfig(request.user, request.params.chainid, request.method);
      if (typeof request.chaintemplate.chain=== 'string') {
            request.chaintemplate.chain = JSON.parse(request.chaintemplate.chain);
      }
      const chaindef = JSON.parse(JSON.stringify(request.chaintemplate.chain)); // create a clone , instead of modiying the in memory objct on nod-cache
      // eslint-disable-next-line no-restricted-syntax
      for (const joblet of chaindef) {
            const flattendchain = flatten(bodychain);
            Object.keys(joblet.datacontent).forEach((key) => {
                  joblet.datacontent[key] = flattendchain[joblet.datacontent[key]];
            });
            request.datacontent = joblet.datacontent;
            request.template = await this.ResourceConfig(request.user, joblet.stub, joblet.method);

            const valid = await request.ajvHandler(request, reply);
            if (valid !== true) {
                  reply.code(422).send(reply.replyHandler(valid, request.id));
                  flag = false;
                  break;
            } else {
                  const data = await this[request.template.type](request, reply);
                  bodychainindex += 1;
                  bodychain[bodychainindex] = data;
            }
      }
      if (flag) {
            reply.send(reply.replyHandler(bodychain[bodychainindex], request.id));
      }
}

exports.job = job;
exports.chain = chain;
