/*
* Copyright 2021 Datatrucker.io Inc , Ontario , Canada
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*     http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/ 

const groupsSearch = {
      querystring: {
            groupname: {type: 'string', minimum: 4, pattern: '^[A-za-z0-9]+$'}
      }
};

const groups = {
      body: {
            type: 'object',
            required: ['groupname', 'level', 'tenantname', 'enabled'],
            properties: {
                  groupname: {type: 'string', minimum: 4, pattern: '^[A-za-z0-9]+$'},
                  level: {type: 'string', enum: ['Tenant_Reader', 'Tenant_Author']},
                  tenantname: {type: 'string', pattern: '^[A-Za-z0-9]+$', minimum: 4},
                  enabled: {type: 'boolean'},
                  type: {type: 'string', enum: ['local','keycloak']}
            }
      }
};

const groupsUpdate = {
      params: {
            type: 'object',
            properties: {
                  groupname: {type: 'string', minimum: 4, pattern: '^[A-za-z0-9]+$'}
            }
      },
      body: {
            type: 'object',
            required: ['level', 'tenantname', 'enabled','type'],
            properties: {
                  level: {type: 'string', enum: ['Tenant_Reader', 'Tenant_Author']},
                  tenantname: {type: 'string', pattern: '^[A-Za-z0-9]+$', minimum: 4},
                  enabled: {type: 'boolean'},
                  type: {type: 'string', enum: ['local','keycloak']}
            }
      }
};

const groupsDelete = {
      params: {
            type: 'object',
            properties: {
                  groupname: {type: 'string', minimum: 4, pattern: '^[A-za-z0-9]+$'}
            }
      }
};

const tenants = {
      querystring: {
            groupname: {type: 'string', minimum: 4, pattern: '^[A-za-z0-9]+$'}
      }
};

exports.groupsSearch = groupsSearch;
exports.groups = groups;
exports.groupsUpdate = groupsUpdate;
exports.groupsDelete = groupsDelete;
exports.tenants = tenants;
